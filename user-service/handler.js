'use strict';
const { DynamoDBClient, QueryCommand, ScanCommand, PutItemCommand, GetItemCommand } = require("@aws-sdk/client-dynamodb");
//const { v4: uuidv4 } = require('uuid');
//import { v4 as uuidv4 } from 'uuid';
const client = new DynamoDBClient({ region: "us-east-2" });

module.exports.loginUser = async (event) => {
  let loginSuccess = false;
  const result = JSON.parse(event.body);
  const input = {
    "ExpressionAttributeValues": {
      ":a": {
        "S": result.username
      }
    },
    "FilterExpression": "username = :a",
    "TableName": "User"
  };
  /*const command = new GetItemCommand({
    TableName: "User",
    Key: {
      "username": {
      "S": 'username'
    },
    "password": {
      "S": 'password'
    }},
  });*/
  const command = new ScanCommand(input);

  const response = await client.send(command);
  let loginAttempt;

  if(response.Items.length !== 0) {
    loginAttempt = response.Items[0];
    if(loginAttempt.password.S === result.password) {
      loginSuccess = true;
    }
  }
  //const currentUser = response.Items[0];

  return {
    statusCode: loginSuccess ? 200 : 404,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify(
        loginSuccess ? loginAttempt : 'login unsuccessful',
        //response.Items[0]
    ),
  };
};

module.exports.getUserInfo = async (event) => {
  const result = JSON.parse(event.body);

  const command = new GetItemCommand({
    TableName: "User",
    Key: {
      "username": {
      "S": result.username
    },
    "password": {
      "S": result.password
    }},
  });
  const response = await client.send(command);
  //const selectedProduct = response.Item;
  //let stockCommand = new ScanCommand(stockParams);
  //const res2 = await client.send(stockCommand);
  //let stockList = res2.Items; 

  /*let joinedProductsList = [];
  for(let i = 0; i < productsList.length; i++) {
    for(let j = 0; j < stockList.length; j++) {
      if(JSON.stringify(productsList[i].id) === JSON.stringify(stockList[j].product_id)) {
        joinedProductsList.push({
          id: productsList[i].id,
          title: productsList[i].title,
          description: productsList[i].description,
          price: productsList[i].price,
          count: stockList[i].count
        })
        break;
      }
    }
  }*/

  return {
    //statusCode: 200,
    statusCode: response ? 200 : 404,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify(
      //joinedProductsList
      //response
      response ? response : 'user not found'
    ),
  };
}

module.exports.createUser = async (event) => {
  const result = JSON.parse(event.body);
  //const randomId = uuidv4();
  const randomId = 'randomId' + Math.floor(Math.random() * 100);
  const randomUsername = result.firstName + '_' + result.lastName + Math.floor(Math.random() * 100);
  const randomPassword = 'tempPassword' + Math.floor(Math.random() * 100);
  //const randomUserId = uuidv4();
  //const randomToken = uuidv4();
  const randomToken = 'tokenSession' + Math.floor(Math.random() * 100);

  const params = {
    TableName: "User",
    Item: {
      id: { S: randomId },
      firstName: { S: result.firstName },
      lastName: { S: result.lastName },
      username: { S: randomUsername },
      email: { S: result.email },
      photo: { S: 'photo-link' },
      password: { S: randomPassword },
      isActive: { BOOL: 'true' },
      role: { S: result.role },
      token: { S: randomToken }
    },
  };

  const roleParams = {
    TableName: "Student",
    Item: {
      id: { S: randomId },
      userId: { S: 'randomUserId' },
      dateOfBirth: { S: result.dateOfBirth },
      address: { S: result.address },
    },
  };

  let userCommand = new PutItemCommand(params);
  const userRes = await client.send(userCommand);
  let studentCommand = new PutItemCommand(roleParams);
  const studentRes = await client.send(studentCommand);
  let newItem = userRes;

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify(
      {
        message: 'Registered user',
        newItem
      }
    ),
  };
}